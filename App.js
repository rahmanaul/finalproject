import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Spotify from './src/screens/SpotifyTrends';
import Stackk from './src/Routes/Stack';

export default function App() {
  return (
    <View style={styles.container}>
      <Stackk />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
