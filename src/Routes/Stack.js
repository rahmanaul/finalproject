import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Spotify from '../screens/SpotifyTrends';
import DetailSpotify from '../screens/DetailSongScreen';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Twitter from '../screens/Twitter';
import News from '../screens/News';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function Stackk() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='IndoTrend' component={TabBawah} />
        {/* <Stack.Screen name='Home' component={Spotify} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
}

function TabBawah() {
  return (
    <Tab.Navigator>
      <Tab.Screen name='Spotify' component={Spotify} />
      <Tab.Screen name='Twitter' component={Twitter} />
      <Tab.Screen name='News' component={News} />
    </Tab.Navigator>
  );
}

export default Stackk;
