import React from 'react';
import { View, Text } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import ListSong from './ListSong';
import Separator from './Separators';

export default function listingSong({ items, navigation }) {
  return (
    <FlatList
      style={{ backgroundColor: '#20639B', margin: 30 }}
      data={items}
      renderItem={({ item }) => (
        <ListSong item={item} navigation={navigation} />
      )}
      keyExtractor={(item) => item.id}
      ItemSeparatorComponent={() => <Separator />}
      ListEmptyComponent={() => <Text>No Songs</Text>}
    />
  );
}
