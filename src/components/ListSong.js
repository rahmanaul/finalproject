import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

export default function ListSong({ item: { artists, title, position } }) {
  return (
    <TouchableOpacity
      style={styles.songCard}
      onPress={() => {
        this.props.navgation.navigate('Details');
      }}
    >
      <View style={styles.container}>
        <Text>{position}</Text>
        <View style={styles.song}>
          <Text>{artists} - </Text>
          <Text>{title}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
  },
  song: {
    paddingLeft: 20,
  },
  songCard: {
    backgroundColor: '#3CAEA3',
    padding: 5,
    borderRadius: 10,
  },
});
