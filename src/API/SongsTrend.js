const apiPrefix = 'https://api.spotify.com/v1';

export default async ({ token }) => {
  const uri = `${apiPrefix}/playlists/1eh63KLRSCM8CiWx2K5Ick/tracks?market=ID&limit=20`;
  console.log('search begin, uri =', uri, 'token =', token);
  const res = await fetch(uri, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
  const json = await res.json();
  console.log('search got json', json);

  if (!res.ok) {
    return [];
  }
  const { items: items } = json;
  // const items = json.tracks.items;
  return items.map((item, index) => ({
    position: index + 1,
    id: item.track.id,
    artists: item.track.artists[0].name,
    title: item.track.name,
    album: item.track.album.name,
    imageUri: item.track.album.images,
    play: item.track.external_urls.spotify,
  }));
  console.log('search end');
};
