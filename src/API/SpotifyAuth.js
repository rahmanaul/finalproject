import base64 from 'react-native-base64';

const apiPrefix = 'https://accounts.spotify.com/api';
const clientID = '4fd5e642a0da4bd5aed68683d67fbbf2';
const clientSecret = 'eeae5769ff63455595d443c1275d9558';

const base64credentials = base64.encode(clientID + ':' + clientSecret);

export default async () => {
  console.log('token begin');
  const res = await fetch(`${apiPrefix}/token`, {
    method: 'POST',
    headers: {
      Authorization: `Basic ${base64credentials}`,
      'Content-Type': 'application/x-www-form-urlencoded',
    },
    body: 'grant_type=client_credentials',
  });
  const json = await res.json();
  const newToken = json.access_token;
  console.log('token is', newToken);
  return newToken;
};
