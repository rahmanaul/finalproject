import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Button,
  ActivityIndicator,
} from 'react-native';
import auth from '../API/SpotifyAuth';
import songsTrend from '../API/SongsTrend';
import { FlatList } from 'react-native-gesture-handler';
import ListingSong from '../components/ListingSong';

export default class TwitterTrends extends Component {
  state = {
    loading: true,
    token: null,
    songs: [],
    isFetching: false,
    isEmpty: false,
    isTokenFetching: false,
  };

  async load() {
    const { songs, token, isFetching, isEmpty } = this.state;

    if (isFetching || isEmpty) return;

    this.setState({ isFetching: true });
    const newSongs = await songsTrend({
      token,
    });
    // console.log(newSongs);
    if (newSongs.length === 0) {
      console.log('There is no songs found, there may be an error');
      this.setState({ isEmpty: true });
    }

    this.setState({
      isFetching: false,
      songs: [...songs, ...newSongs],
    });
    console.log('array siap', this.state.songs);
  }

  async refreshToken() {
    this.setState({
      isTokenFetching: true,
    });
    const newToken = await auth();
    this.setState({
      token: newToken,
      isTokenFetching: false,
    });
  }

  async componentDidMount() {
    await this.refreshToken();
    await this.load();
  }

  render() {
    const { songs, isFetching } = this.state;
    return (
      <View>
        {isFetching && songs.length === 0 ? (
          <ActivityIndicator />
        ) : (
          <ListingSong items={songs} navigation={this.props.navigation} />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({});
