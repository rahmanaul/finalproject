import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';

const apiKey = 'efff1d0c9dcc4ba7baa7db8054d7e039';

export default class News extends Component {
  state = {
    loading: true,
    news: [],
    isFetching: false,
    isEmpty: false,
    isTokenFetching: false,
  };

  async load() {
    const news = await fetch(
      `http://newsapi.org/v2/top-headlines?country=id&apiKey=${apiKey}`,
      {
        method: 'GET',
      }
    );
    const json = await news.json();
    // console.log('search got json', json);
    const { articles: items } = json;
    const newNews = await items.map((item, index) => ({
      id: index + 1,
      author: item.author,
      title: item.title,
      published: item.publishedAt,
      description: item.description,
      url: item.url,
      urlToImage: item.urlToImage,
    }));
    this.setState({
      isFetching: false,
      news: [...news, ...newNews],
    });
  }

  async componentDidMount() {
    await this.load();
  }

  render() {
    const { news } = this.state;
    return (
      <FlatList
        data={news}
        renderItem={({ item }) => {
          return <Text>{item.title}</Text>;
        }}
        keyExtractor={(item) => item.id}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  item: {
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});
